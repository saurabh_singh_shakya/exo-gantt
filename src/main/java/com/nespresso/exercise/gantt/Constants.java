package com.nespresso.exercise.gantt;

/**
 * Constants Class
 * 
 * @author saurabh.singh.shakya
 *
 */
public class Constants {
	public static final Character WAITING = '.';
	public static final String DELEMETER = " -> ";
}
