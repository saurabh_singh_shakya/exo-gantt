package com.nespresso.exercise.gantt;

import java.util.HashMap;
import java.util.Map;

/**
 * The class to compute the Gantt Plan Chart
 * 
 * @author saurabh.singh.shakya
 *
 */
public class Gantt {

	private Map<String, Task> taskPlan = new HashMap<String, Task>();
	private Integer maxDuration = Integer.valueOf(0);

	/**
	 * method to add task into the task plan
	 * 
	 * @param taskName
	 *            name of task
	 * @param startWeek
	 *            start week (0 if no delay)
	 * @param durationInWeeks
	 *            duration of task in weeks
	 * @param dependentTaskNames
	 *            dependent tasks
	 * @throws CyclicDependencyException
	 *             throws runtime exception if the dependency of tasks is
	 *             cyclic.
	 */
	public void addTask(String taskName, int startWeek, int durationInWeeks,
			String... dependentTaskNames) throws CyclicDependencyException {
		Integer delay = Integer.valueOf(0);
		String actualDependentTaskName = null;
		for (String dependentTaskName : dependentTaskNames) {
			validateTasks(taskName, dependentTaskName);
			Task dependantTask = taskPlan.get(dependentTaskName);
			if (delay < dependantTask.getStartWeek()
					+ dependantTask.getDurationInWeeks()) {
				delay = dependantTask.getStartWeek()
						+ dependantTask.getDurationInWeeks();
				actualDependentTaskName = dependentTaskName;
			}
		}
		startWeek += delay;
		Task task = new Task(Character.toUpperCase(taskName.charAt(0)),
				startWeek, durationInWeeks);
		// to check the actual dependency on task out of all dependent tasks
		if (dependentTaskNames.length != 0) {
			task.setDependentTask(actualDependentTaskName);
		}
		taskPlan.put(taskName, task);
		calculateMaxTime(startWeek, durationInWeeks);
	}

	private void validateTasks(String taskName, String dependentTaskName) {
		if (taskPlan.containsKey(taskName)) {
			throw new CyclicDependencyException(createExceptionMessage(
					taskName, dependentTaskName));
		}
	}

	private void calculateMaxTime(int startWeek, int durationInWeeks) {
		if (maxDuration < startWeek + durationInWeeks) {
			maxDuration = startWeek + durationInWeeks;
		}
	}

	/**
	 * method to display the gantt chart of task plan
	 * 
	 * @param taskName
	 *            name of task
	 * @return gantt chart
	 */
	public String showPlanFor(String taskName) {
		StringBuilder plan = new StringBuilder();
		Task task = taskPlan.get(taskName);
		// loop for waiting time before starting task
		for (int i = 0; i < task.getStartWeek(); i++) {
			plan.append(Constants.WAITING);
		}
		// loop for task
		for (int i = 0; i < task.getDurationInWeeks(); i++) {
			plan.append(task.getTaskSymbol());
		}
		// loop for waiting time after task completion
		for (int i = 0; i < maxDuration
				- (task.getStartWeek() + task.getDurationInWeeks()); i++) {
			plan.append(Constants.WAITING);
		}
		return plan.toString();
	}

	/**
	 * method to create the message for cyclic exception
	 * 
	 * @param taskName
	 *            name of task
	 * @param dependentTaskName
	 *            name of dependent task
	 * @return message
	 */
	private String createExceptionMessage(String taskName,
			String dependentTaskName) {
		StringBuilder cyclePath = new StringBuilder("Cycle detected : ");
		cyclePath.append(taskName);
		cyclePath.append(Constants.DELEMETER);
		cyclePath.append(dependentTaskName);
		while (null != taskPlan.get(dependentTaskName).getDependentTask()) {
			dependentTaskName = taskPlan.get(dependentTaskName)
					.getDependentTask();
			cyclePath.append(Constants.DELEMETER);
			cyclePath.append(dependentTaskName);
		}
		return cyclePath.toString();
	}

}
