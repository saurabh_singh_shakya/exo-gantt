package com.nespresso.exercise.gantt;

/**
 * The class to maintain the task start week, duration of task and dependencies
 * on tasks.
 * 
 * @author saurabh.singh.shakya
 *
 */
public class Task {

	private Character taskSymbol;
	private Integer startWeek;
	private Integer durationInWeeks;
	private String dependentTask;

	public Task(Character taskSymbol, Integer startWeek, Integer durationInWeeks) {
		this.taskSymbol = taskSymbol;
		this.startWeek = startWeek;
		this.durationInWeeks = durationInWeeks;
	}

	public Task(Character taskSymbol, Integer startWeek,
			Integer durationInWeeks, String dependentTask) {
		this.taskSymbol = taskSymbol;
		this.startWeek = startWeek;
		this.durationInWeeks = durationInWeeks;
		this.dependentTask = dependentTask;
	}

	public Character getTaskSymbol() {
		return taskSymbol;
	}

	public void setTaskSymbol(Character taskSymbol) {
		this.taskSymbol = taskSymbol;
	}

	public Integer getStartWeek() {
		return startWeek;
	}

	public void setStartWeek(Integer startWeek) {
		this.startWeek = startWeek;
	}

	public Integer getDurationInWeeks() {
		return durationInWeeks;
	}

	public void setDurationInWeeks(Integer durationInWeeks) {
		this.durationInWeeks = durationInWeeks;
	}

	public String getDependentTask() {
		return dependentTask;
	}

	public void setDependentTask(String dependentTask) {
		this.dependentTask = dependentTask;
	}

}
